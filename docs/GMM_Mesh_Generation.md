## Gmm Mesh Generation

The topology of the coarsening between heirarchy levels is done by establishing connectivity via the `finerNodes` and `coarserNodes` vectors of the node classes.

Specifically when dyadically corasening between levels:
- **a given coraser node** gets the index each finer node within its immediate approximity, there are potentially 27 (3^3) finer nodes in this region. These node IDs are stored in lexicographic order and -1 is used to indicate that a node doesn't exist.
- **a given finer node** gets the index of each coarser node within its immediate approximity, this is equal to the set of coarse nodes that have its index in their `finerNodes` region and is potentially 8 (2^3) coarser nodes. The nodes are stored in lexicographic order and -1 is used to indicate that the node doesn't exist.

The location of a `finerNode` can be categories by the number of Coarser nodes it contributes to. There are only 4 possible types and these can be describe by whether they lie in the centre on an element/cell/cube (defined by 8 nodes) or on one of its surface components - vertex, edge, plane.

| Type | Number of Coarse Node Contributions |
| --- | --- | 
| Cell | 8 |
| Plane | 4 |
| Edge | 2 |
| Vertex | 1 |

For a given coarse node if we visit each of the finer nodes in its immediate approximaty in lexicographic order then the order of the type of nodes we will visit is:

```
c, p, c, p, e, p, c, p, c,  
p, e, p, e, v, e, p, e, p,  
c, p, c, p, e, p, c, p, c
```
For each finer node in the lexicographic ordering above we can determine in advance the position that the coarse nodes index would be placed in the finer nodes `coarseNodes` array. 

```
7, 3, 6, 3, 1, 2, 5, 2, 4,  
3, 1, 2, 1, 0, 0, 1, 0, 0,  
3, 1, 2, 1, 0, 0, 1, 0, 0
```
----

*Side note*:  
if we take the dual graph of the coarse cells then this gives us the boundaries of the finer nodes surrounding each coarse node (because the coarse nodes which are vertices in the primal graph become cells in the dual graph and planes in the primal graph become edges in the dual graph etc.) leading to the ordering of the types of nodes we visit as:

```
v, e, v, e, p, e, v, e, v,
e, p, e, p, c, p, e, p, e,
v, e, v, e, p, e, v, e, v
```
which may be conceptually simpler to think about. Remembering that the number of coarse node contributions need to change also i.e. vertices == 8, cell == 1 etc.

----


