Voxel to Fintie Element Meshes (VoxelToFEMesh)
=====
This is library for the conversion of voxels to various finite element meshes. Please note this is very much still under construction.

Currently the library only supports conversion from the [VoxelGrid](https://bitbucket.org/l90lpa/voxelgrid) representation of voxels to 2 non-standard finite element mesh types. I.e. it doesn't currently support conversion to any of the standard mesh types such as Gmsh and Vtk unstructured grids.

Finite Element Mesh Types:

- *FEMesh*: a uniform hexahedral finite element mesh with most of the basic features
- *GMMMesh*: a uniform hexahedral dyadically coarserned heirachical finite element mesh for use in the geometric multigrid method.

The provided mesh type 'FEMesh' and 'GMMMesh' both have an AoS and a SoA version so that one can use the data layout that best supports their needs. For example the SoA layout is much more GPU friendly.


How to build (using CMake)?
-----

The dependencies of this project are: 

- VoxelGrid
- GLM

VoxelGrid is currently a submodule and so to satisfy this dependency is as simple as using `--init --recursive` when cloning this repo. I.e. `git clone --init --recursive this_repo`

For all other dependencies I recommend using VCPKG and simply supplying the VCPKG toolchain file at the command line when running cmake to generate build files. I.e. having cloned and booststrapped VCPKG, then install GLM through VCPKG, we can then do somethinf like `cmake .. -DCMAKE_TOOLCHAIN_FILE="path/to/vcpkg.cmake"` to generate the build files and `cmake . --build` to build the project.

How to consume (using CMake)?
-----
The CMake build system files provided in this project make available the target library `FEMeshGen`. This target has appropriate properties set for including the relevant dependencies. Thus a simple why to consume this library into your project is to simply add the CMake command `add_subdirectory(path/to/root/dir/of/FEMeshGen)` to an appropriate place in your build system files and then use either of the CMake commands `target_include_directories(YourTarget PRIVATE FEMeshGen)` or `include_directories(FEMeshGen)`.