
#pragma once 

#include "EigenTypes.h"
#include "GMMMesh/GMMMeshConversion.h"

template<typename FP_T>
bool operator ==(const gmm::Element<FP_T>& lhs, const gmm::Element<FP_T>& rhs) {

	bool are_equal = lhs.id == rhs.id;
	are_equal = (are_equal && std::equal(lhs.nodes.begin(), lhs.nodes.end(), rhs.nodes.begin()));
	are_equal = (are_equal && (lhs.initialConditions.elasticModulus == rhs.initialConditions.elasticModulus));
	are_equal = (are_equal && (lhs.initialConditions.density == rhs.initialConditions.density));

	return are_equal;
}

template<typename FP_T>
bool operator ==(const gmm::LeafNode<FP_T>& lhs, const gmm::LeafNode<FP_T>& rhs) {

	bool are_equal = lhs.id == rhs.id;
	are_equal = (are_equal && lhs.gridPosition == rhs.gridPosition);
	are_equal = (are_equal && std::equal(lhs.elements.begin(), lhs.elements.end(), rhs.elements.begin()));
	are_equal = (are_equal && std::equal(lhs.adjNodes.begin(), lhs.adjNodes.end(), rhs.adjNodes.begin()));
	are_equal = (are_equal && std::equal(lhs.coarseNodes.begin(), lhs.coarseNodes.end(), rhs.coarseNodes.begin()));
	are_equal = (are_equal && lhs.initialPosition == rhs.initialPosition);
	are_equal = (are_equal && lhs.initialConditions.initialForce == rhs.initialConditions.initialForce);
	are_equal = (are_equal && lhs.initialConditions.is_fixedPosition == rhs.initialConditions.is_fixedPosition);

	return are_equal;
}

template<typename FP_T>
bool operator ==(const gmm::BranchNode<FP_T>& lhs, const gmm::BranchNode<FP_T>& rhs) {

	bool are_equal = lhs.id == rhs.id;
	are_equal = (are_equal && lhs.gridPosition == rhs.gridPosition);
	are_equal = (are_equal && std::equal(lhs.fineNodes.begin(), lhs.fineNodes.end(), rhs.fineNodesB.begin()));
	are_equal = (are_equal && std::equal(lhs.adjNodes.begin(), lhs.adjNodes.end(), rhs.adjNodes.begin()));
	are_equal = (are_equal && std::equal(lhs.coarseNodes.begin(), lhs.coarseNodes.end(), rhs.coarseNodes.begin()));

	return are_equal;
}


template<typename FP_T, Eigen::StorageOptions StorageLayout>
bool checkElementsAreEqual(const gmm::ElementContainer_AOS<FP_T>& elementsAOS, const gmm::ElementContainer_SOA<FP_T, StorageLayout>& elementsSOA) {
	bool elementsAreEqual = true;
	for (size_t index = 0; index < elementsAOS.size(); ++index) {
		elementsAreEqual = (elementsAreEqual && elementsAOS[index] == gmm::getElement(index, elementsSOA));
	}
	return elementsAreEqual;
}

template<typename FP_T, Eigen::StorageOptions StorageLayout>
bool checkElementsAreEqual(const gmm::ElementContainer_SOA<FP_T, StorageLayout>& elementsSOA, const gmm::ElementContainer_AOS<FP_T>& elementsAOS) {
	return checkElementsAreEqual(elementsAOS, elementsSOA);
}

template<typename FP_T, Eigen::StorageOptions StorageLayout>
bool checkNodesAreEqual(const gmm::LeafNodeContainer_AOS<FP_T>& nodesAOS, const gmm::LeafNodeContainer_SOA<FP_T, StorageLayout>& nodesSOA) {
	bool nodesAreEqual = true;
	for (size_t index = 0; index < nodesAOS.size(); ++index) {
		nodesAreEqual = (nodesAreEqual && nodesAOS[index] == gmm::getNode(index, nodesSOA));
	}
	return nodesAreEqual;
}

template<typename FP_T, Eigen::StorageOptions StorageLayout>
bool checkNodesAreEqual(const gmm::LeafNodeContainer_SOA<FP_T, StorageLayout>& nodesSOA, const gmm::LeafNodeContainer_AOS<FP_T>& nodesAOS) {
	return checkNodesAreEqual(nodesAOS, nodesSOA);
}

template<typename FP_T, Eigen::StorageOptions StorageLayout>
bool checkNodesAreEqual(const gmm::BranchNodeContainer_AOS<FP_T>& nodesAOS, const gmm::BranchNodeContainer_SOA<FP_T, StorageLayout>& nodesSOA) {
	bool nodesAreEqual = true;
	for (size_t index = 0; index < nodesAOS.size(); ++index) {
		nodesAreEqual = (nodesAreEqual && nodesAOS[index] == gmm::getNode(index, nodesSOA));
	}
	return nodesAreEqual;
}

template<typename FP_T, Eigen::StorageOptions StorageLayout>
bool checkNodesAreEqual(const gmm::BranchNodeContainer_SOA<FP_T, StorageLayout>& nodesSOA, const gmm::BranchNodeContainer_AOS<FP_T>& nodesAOS) {
	return checkNodesAreEqual(nodesAOS, nodesSOA);
}