
#include "catch.hpp"

#include "GMMMesh/VoxToGMMMesh.h"

#include "GMMMesh/GMMMeshValidator.h"

using Index_type = uint32_t;
using Block_type = uint32_t;
using Bitgrid_type = BitGrid3<Block_type, Index_type>;
using FP_type = float;

using VGrid = VoxelGrid<Bitgrid_type, FP_type>;


SCENARIO("Constructing a geometric multigrid finite element mesh from a voxel grid", "[GMM_Mesh_Construction]") {

	// TODO: re-enable after issue #3: https://bitbucket.org/l90lpa/voxeltofemesh/issues/3
	//GIVEN("An empty voxel grid") {
	//	VGrid vGrid{};

	//	WHEN("We build a finite element mesh") {

	//		auto gmmMesh = gmm::voxToGMMMesh<float>(vGrid);

	//		THEN("The mesh contains no elements") {
	//			REQUIRE(gmmMesh.leafLevel.elements.empty());
	//		}
	//		AND_THEN("The mesh contains no lv0 nodes") {
	//			REQUIRE(gmmMesh.leafLevel.nodes.empty());
	//		}
	//		AND_THEN("The mesh contains no other GM levels") {
	//			REQUIRE(gmmMesh.branchLevels.empty());
	//		}
	//		AND_THEN("The mesh validator confirms that the mesh is correct") {
	//			auto result = meshValidator(gmmMesh);
	//			REQUIRE(result == gmm::CheckResult::Correct);
	//		}
	//	}
	//}

	GIVEN("An voxel grid with no active voxels") {

		const Index_type axisResolution = 4;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 4.0, 4.0, 4.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		WHEN("We build a finite element mesh") {

			auto gmmMesh = gmm::voxToGMMMesh<float>(vGrid);

			THEN("The mesh contains no elements") {
				REQUIRE(gmmMesh.leafLevel.elements.empty());
			}
			AND_THEN("The mesh contains no lv0 nodes") {
				REQUIRE(gmmMesh.leafLevel.nodes.empty());
			}
			AND_THEN("The mesh contains no other GM levels") {
				REQUIRE(gmmMesh.branchLevels.empty());
			}
			AND_THEN("The mesh validator confirms that the mesh is correct") {
				auto result = meshValidator(gmmMesh);
				REQUIRE(result == gmm::CheckResult::Correct);
			}
		}
	}

	GIVEN("An voxel grid of 1 element") {

		const Index_type axisResolution = 1;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 1.0, 1.0, 1.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		// Fill the voxel grid		
		vGrid[glm::tvec3<Index_type>{ 0,0,0 }] = ElementState::Active;

		WHEN("We build a finite element mesh") {

			auto gmmMesh = gmm::voxToGMMMesh<float>(vGrid);

			THEN("The mesh contains 1 element") {
				REQUIRE(gmmMesh.leafLevel.elements.size() == 1);
			}
			AND_THEN("The mesh contains 8 lv0 nodes") {
				REQUIRE(gmmMesh.leafLevel.nodes.size() == 8);
			}
			AND_THEN("The mesh contains no other GM levels") {
				REQUIRE(gmmMesh.branchLevels.empty());
			}
			AND_THEN("The mesh validator confirms that the mesh is correct") {
				auto result = meshValidator(gmmMesh);
				REQUIRE(result == gmm::CheckResult::Correct);
			}
		}
	}

	GIVEN("A 3^3 voxel grid") {

		const Index_type axisResolution = 3;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 3.0, 3.0, 3.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		// Fill the voxel grid		
		vGrid[glm::tvec3<Index_type>{ 1,1,0 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 0,1,1 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 1,0,1 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 1,2,1 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 2,1,1 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 1,1,1 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 1,1,2 }] = ElementState::Active;

		WHEN("We build a finite element mesh") {

			auto gmmMesh = gmm::voxToGMMMesh<float>(vGrid);

			THEN("The mesh contains 7 elements") {
				REQUIRE(gmmMesh.leafLevel.elements.size() == 7);
			}
			AND_THEN("The mesh contains 32 lv0 nodes") {
				REQUIRE(gmmMesh.leafLevel.nodes.size() == 32);
			}
			AND_THEN("The mesh contains 2 other GM levels with 20 and 8 nodes respectively") {
				REQUIRE(gmmMesh.branchLevels.size() == 2);

				REQUIRE(gmmMesh.branchLevels[0].nodes.size() == 20);
				REQUIRE(gmmMesh.branchLevels[1].nodes.size() == 8);
			}
			AND_THEN("The mesh validator confirms that the mesh is correct") {
				auto result = meshValidator(gmmMesh);
				REQUIRE(result == gmm::CheckResult::Correct);
			}
		}
	}

	GIVEN("A 6^3 voxel grid") {

		const Index_type axisResolution = 6;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 6.0, 6.0, 6.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		// Fill the voxel grid		
		vGrid[glm::tvec3<Index_type>{ 2, 2, 0 }] = ElementState::Active; // centre
		vGrid[glm::tvec3<Index_type>{ 3, 2, 0 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 2, 3, 0 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 3, 3, 0 }] = ElementState::Active;

		vGrid[glm::tvec3<Index_type>{ 2, 2, 1 }] = ElementState::Active; // centre
		vGrid[glm::tvec3<Index_type>{ 3, 2, 1 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 2, 3, 1 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 3, 3, 1 }] = ElementState::Active;

		vGrid[glm::tvec3<Index_type>{ 2, 2, 2 }] = ElementState::Active; // centre
		vGrid[glm::tvec3<Index_type>{ 3, 2, 2 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 2, 3, 2 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 3, 3, 2 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 2, 0, 2 }] = ElementState::Active; // bottom
		vGrid[glm::tvec3<Index_type>{ 3, 0, 2 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 2, 1, 2 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 3, 1, 2 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 0, 2, 2 }] = ElementState::Active; // left
		vGrid[glm::tvec3<Index_type>{ 1, 2, 2 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 0, 3, 2 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 1, 3, 2 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 2, 4, 2 }] = ElementState::Active; // top
		vGrid[glm::tvec3<Index_type>{ 3, 4, 2 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 2, 5, 2 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 3, 5, 2 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 4, 2, 2 }] = ElementState::Active; // right
		vGrid[glm::tvec3<Index_type>{ 5, 2, 2 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 4, 3, 2 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 5, 3, 2 }] = ElementState::Active;

		vGrid[glm::tvec3<Index_type>{ 2, 2, 3 }] = ElementState::Active; // centre
		vGrid[glm::tvec3<Index_type>{ 3, 2, 3 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 2, 3, 3 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 3, 3, 3 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 2, 0, 3 }] = ElementState::Active; // bottom
		vGrid[glm::tvec3<Index_type>{ 3, 0, 3 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 2, 1, 3 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 3, 1, 3 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 0, 2, 3 }] = ElementState::Active; // left
		vGrid[glm::tvec3<Index_type>{ 1, 2, 3 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 0, 3, 3 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 1, 3, 3 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 2, 4, 3 }] = ElementState::Active; // top
		vGrid[glm::tvec3<Index_type>{ 3, 4, 3 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 2, 5, 3 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 3, 5, 3 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 4, 2, 3 }] = ElementState::Active; // right
		vGrid[glm::tvec3<Index_type>{ 5, 2, 3 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 4, 3, 3 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 5, 3, 3 }] = ElementState::Active;

		vGrid[glm::tvec3<Index_type>{ 2, 2, 4 }] = ElementState::Active; // centre
		vGrid[glm::tvec3<Index_type>{ 3, 2, 4 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 2, 3, 4 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 3, 3, 4 }] = ElementState::Active;

		vGrid[glm::tvec3<Index_type>{ 2, 2, 5 }] = ElementState::Active; // centre
		vGrid[glm::tvec3<Index_type>{ 3, 2, 5 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 2, 3, 5 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 3, 3, 5 }] = ElementState::Active;

		WHEN("We build a finite element mesh") {

			auto gmmMesh = gmm::voxToGMMMesh<float>(vGrid);

			THEN("The mesh contains 56 elements") {
				REQUIRE(gmmMesh.leafLevel.elements.size() == 56);
			}
			AND_THEN("The mesh contains 135 lv0 nodes") {
				REQUIRE(gmmMesh.leafLevel.nodes.size() == 135);
			}
			AND_THEN("The mesh contains 3 other GM levels with 32, 20 and 8 nodes respectively") {
				REQUIRE(gmmMesh.branchLevels.size() == 3);

				REQUIRE(gmmMesh.branchLevels[0].nodes.size() == 32);
				REQUIRE(gmmMesh.branchLevels[1].nodes.size() == 20);
				REQUIRE(gmmMesh.branchLevels[2].nodes.size() == 8);
			}
			AND_THEN("The mesh validator confirms that the mesh is correct") {
				auto result = meshValidator(gmmMesh);
				REQUIRE(result == gmm::CheckResult::Correct);
			}
		}
	}

}
