#include "catch.hpp"

#include "GMMMesh/VoxToGMMMesh.h"
#include "GMMMesh/GMMMeshConversion.h"
#include "TestUtilities/GMMMeshConversion_testUtilities.h"

#include <Eigen/Dense>

#include <iostream>
#include <algorithm>

using Index_type = uint32_t;
using Block_type = uint32_t;
using Bitgrid_type = BitGrid3<Block_type, Index_type>;
using FP_type = float;

using VGrid = VoxelGrid<Bitgrid_type, FP_type>;

SCENARIO("Converting a GMMMesh_AOS to GMMMesh_SOA", "[GMM_Mesh_Conversion]") {

	// TODO: re-enable after issue #3: https://bitbucket.org/l90lpa/voxeltofemesh/issues/3
	//GIVEN("an empty gmmMesh") {

	//	VGrid vGrid{};
	//	auto gmmMesh = gmm::voxToGMMMesh<float>(vGrid);

	//	WHEN("we convert it to a gmmMeshSOA") {

	//		auto gmmMeshSOA = gmm::to_GMMMesh_SOA<float, Eigen::RowMajor>(gmmMesh);

	//		THEN("the resulting gmmMeshSOA is empty") {
	//			REQUIRE(gmmMeshSOA.empty());
	//		}
	//	}
	//}

	GIVEN("a gmmMesh with no branchLevels and a leaf level with only 1 element and the associated 8 nodes") {

		const Index_type axisResolution = 1;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 1.0, 1.0, 1.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		vGrid[glm::tvec3<Index_type>{ 0, 0, 0 }] = ElementState::Active;

		auto gmmMesh = gmm::voxToGMMMesh<float>(vGrid);

		WHEN("we convert it to a gmmMeshSOA") {

			auto gmmMeshSOA = gmm::to_GMMMesh_SOA<float, Eigen::RowMajor>(gmmMesh);

			THEN("the resulting gmmMeshSOA ...") {

				REQUIRE(gmmMeshSOA.branchLevels.empty());

				auto numElements = gmmMeshSOA.leafLevel.elements.size();
				REQUIRE(numElements.has_value());
				REQUIRE(numElements.value() == 1);

				bool elementsAreEqual = checkElementsAreEqual(gmmMesh.leafLevel.elements, gmmMeshSOA.leafLevel.elements);
				REQUIRE(elementsAreEqual);

				auto numNodes = gmmMeshSOA.leafLevel.nodes.size();
				REQUIRE(numNodes.has_value());
				REQUIRE(numNodes.value() == 8);

				bool nodesAreEqual = checkNodesAreEqual(gmmMesh.leafLevel.nodes, gmmMeshSOA.leafLevel.nodes);
				REQUIRE(nodesAreEqual);
			}
		}
	}

	GIVEN("a gmmMesh with 2 branchLevels and a leaf level with 64 elements and an associated 125 nodes") {

		const Index_type axisResolution = 4;
		const FP_type unitLength = 4.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 4.0, 4.0, 4.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		for(auto& voxel : vGrid) {
			voxel = ElementState::Active;
		}

		auto gmmMesh = gmm::voxToGMMMesh<float>(vGrid);

		WHEN("we convert it to a gmmMeshSOA") {

			auto gmmMeshSOA = gmm::to_GMMMesh_SOA<float, Eigen::RowMajor>(gmmMesh);

			THEN("the resulting gmmMeshSOA ...") {

				auto numElements_sl = gmmMeshSOA.leafLevel.elements.size();
				REQUIRE(numElements_sl.has_value());
				REQUIRE(numElements_sl.value() == 64);

				bool elementsAreEqual = checkElementsAreEqual(gmmMesh.leafLevel.elements, gmmMeshSOA.leafLevel.elements);
				REQUIRE(elementsAreEqual);

				auto numNodes_sl = gmmMeshSOA.leafLevel.nodes.size();
				REQUIRE(numNodes_sl.has_value());
				REQUIRE(numNodes_sl.value() == 125);

				bool nodes_sl_AreEqual = checkNodesAreEqual(gmmMesh.leafLevel.nodes, gmmMeshSOA.leafLevel.nodes);
				REQUIRE(nodes_sl_AreEqual);

				REQUIRE(gmmMeshSOA.branchLevels.size() == 2);

				auto numNodes_cl0 = gmmMeshSOA.branchLevels[0].nodes.size();
				REQUIRE(numNodes_cl0.has_value());
				REQUIRE(numNodes_cl0.value() == 27);

				bool nodes_cl0_AreEqual = checkNodesAreEqual(gmmMesh.leafLevel.nodes, gmmMeshSOA.leafLevel.nodes);
				REQUIRE(nodes_cl0_AreEqual);

				auto numNodes_cl1 = gmmMeshSOA.branchLevels[1].nodes.size();
				REQUIRE(numNodes_cl1.has_value());
				REQUIRE(numNodes_cl1.value() == 8);

				bool nodes_cl1_AreEqual = checkNodesAreEqual(gmmMesh.leafLevel.nodes, gmmMeshSOA.leafLevel.nodes);
				REQUIRE(nodes_cl1_AreEqual);
			}
		}
	}
}