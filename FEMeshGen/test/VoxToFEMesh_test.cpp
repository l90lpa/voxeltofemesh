
#include "catch.hpp"

#include "FEMesh/VoxToFEMesh.h"

using Index_type = uint32_t;
using Block_type = uint32_t;
using Bitgrid_type = BitGrid3<Block_type, Index_type>;
using FP_type = float;

using VGrid = VoxelGrid<Bitgrid_type, FP_type>;

SCENARIO("Constructing a finite element mesh from a voxel grid", "[FE_Mesh_Construction]") {

	GIVEN("An empty voxel grid") {
		VGrid vGrid{};

		WHEN("We build a finite element mesh") {

			auto feMesh = voxToFEMesh(vGrid);

			THEN("The mesh contains 0 elements") {
				REQUIRE(feMesh.elements.empty());
			}
			AND_THEN("The mesh contains 0 nodes") {
				REQUIRE(feMesh.nodes.empty());
			}
		}
	}

	GIVEN("An voxel grid with no active voxels") {

		const Index_type axisResolution = 4;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 4.0, 4.0, 4.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		WHEN("We build a finite element mesh") {

			auto feMesh = voxToFEMesh(vGrid);

			THEN("The mesh contains 0 elements") {
				REQUIRE(feMesh.elements.empty());
			}
			AND_THEN("The mesh contains 0 nodes") {
				REQUIRE(feMesh.nodes.empty());
			}
		}
	}

	GIVEN("An voxel grid of 1 element") {

		const Index_type axisResolution = 1;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 1.0, 1.0, 1.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		vGrid[0] = ElementState::Active;

		WHEN("We build a finite element mesh") {

			auto feMesh = voxToFEMesh(vGrid);

			THEN("The mesh contains 1 elements") {
				REQUIRE(feMesh.elements.size() == 1);
			}
			AND_THEN("The mesh contains 0 nodes") {
				REQUIRE(feMesh.nodes.size() == 8);
			}
		}
	}

	GIVEN("A 3^3 voxel grid") {

		const Index_type axisResolution = 3;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 3.0, 3.0, 3.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };
		// Fill the voxel grid		
		vGrid[glm::tvec3<Index_type>{ 1,1,0 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 0,1,1 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 1,0,1 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 1,2,1 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 2,1,1 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 1,1,1 }] = ElementState::Active;
		vGrid[glm::tvec3<Index_type>{ 1,1,2 }] = ElementState::Active;

		WHEN("We build a finite element mesh") {

			auto feMesh = voxToFEMesh(vGrid);

			THEN("The mesh contains 7 elements") {
				REQUIRE(feMesh.elements.size() == 7);
			}
			AND_THEN("The mesh contains 32 nodes") {
				REQUIRE(feMesh.nodes.size() == 32);
			}
		}
	}

}