
#pragma once 

#include <cstdint>

namespace mcommon {
	static constexpr int32_t EmptyIndex = -1;

} // namespace mcommon
