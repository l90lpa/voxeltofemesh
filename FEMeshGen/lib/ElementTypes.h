#pragma once

#include <cstdint>

enum class ElementType : uint8_t {
	LinearTetrahedral = 4, // node count
	LinearHexaderal = 8    // node count
};