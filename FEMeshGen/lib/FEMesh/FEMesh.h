#pragma once

#include <type_traits>
#include <vector>

#include "Element.h"
#include "Node.h"
#include "ElementTypes.h"

class FEMesh {
public:
	using element_type = Element<float, static_cast<std::underlying_type<ElementType>::type>(ElementType::LinearHexaderal)>;
	using node_type = Node<float, static_cast<std::underlying_type<ElementType>::type>(ElementType::LinearHexaderal)>;

	std::vector<element_type> elements;
	std::vector<node_type> nodes;
};