
#pragma once

#include <vector>

#include "FEMesh/Node.h"

template<typename FP_T, uint8_t NodesPerElement>
using Node_AOS = std::vector<Node<FP_T, NodesPerElement>>;
