
#pragma once

#include <vector>

#include "FEMesh/Element.h"


template<typename FP_T, typename uint8_t NodesPerElement>
using Elements_AOS = std::vector<Element<FP_T, NodesPerElement>>;
