#pragma once

#include <cstdint>
#include <array>

template<typename FP_T, uint8_t NodesPerElement>
class Element {
public:
	FP_T         x, y, z;
	uint32_t   idx;
	uint32_t  material; // material idx (only <= 2^8 = 256 expected)
	std::array<int32_t, NodesPerElement> nodes; // Node<FP_T>* nodes[NodesPerElement];

	constexpr static uint32_t numNodes = NodesPerElement;
};