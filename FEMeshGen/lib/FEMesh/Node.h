#pragma once

#include <cstdint>
#include <array>

template<typename FP_T, uint8_t NodesPerElement>
class Node {
public:
	FP_T           x, y, z;      // coordinates
	//FP_T dx, dy, dz;      // displacements
	uint32_t     idx;			 // index
	//std::vector<uint32_t> constraints;               // Cons<FP_T>* constraint;		 // constraint pointer
	std::array<int32_t, NodesPerElement> elements;  // Elem<FP_T>* elems[NodesPerElement];
};