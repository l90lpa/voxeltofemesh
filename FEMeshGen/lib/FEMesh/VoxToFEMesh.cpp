
#include "VoxToFEMesh.h"

#include "Tools/SpatialHash.h"

#include <unordered_map>
#include <utility>

FEMesh voxToFEMesh(const VoxelGrid<BitGrid3<uint32_t, uint32_t>, float>& voxelGrid) {

	if (voxelGrid.numVoxels() == 0) {
		return {};
	}

	using VGrid = VoxelGrid < BitGrid3<uint32_t, uint32_t>, float >;
	using Position = glm::tvec3<typename VGrid::index_type>;
	using Index = uint32_t; // FEMesh index type. Which should be passed to Elements and Nodes so that they are all the same.
	using PositionToIndexMap = SpatialHashTable_r3<float, Index>;

	FEMesh feMesh;

	PositionToIndexMap elementMap;
	elementMap.reserve(0.65 * voxelGrid.numVoxels()); // TODO: <<< change this to a vector as we should only visit the elements once

	PositionToIndexMap nodeMap;
	nodeMap.reserve(0.65 * voxelGrid.numVoxels()); // TODO: <<< change this to

	Index elementCount = 0;
	Index nodeCount = 0;


	auto it = voxelGrid.begin();
	auto end = voxelGrid.end();

	for (; it != end; ++it) {

		if (*it == ElementState::Active) {

			const auto voxelPosition = (*it).worldPosition();
			// so we've found a voxel:
			// - lets create an element
			// - and 8 nodes
			const auto inserted_element = elementMap.insert({ voxelPosition, elementCount });

			if (inserted_element.second) {

				FEMesh::element_type e;
				e.x = voxelPosition.x;
				e.y = voxelPosition.y;
				e.z = voxelPosition.z;
				e.idx = elementCount;
				e.material;
				e.nodes = { {-1, -1, -1, -1, -1, -1, -1, -1} };// std::array<uint32_t, NodesPerElement> nodes;

				for (size_t k = 0; k < FEMesh::element_type::numNodes; ++k) {

					// Check if the node has already been created
					uint32_t dx = (k % 2);
					uint32_t dy = ((k / 2) % 2); // (k / 2) * (k < 4) + (k / 6) * (k > 4);
					uint32_t dz = (k / 4);

					auto found_node = nodeMap.find(Position{ voxelPosition.x + dx, voxelPosition.y + dy, voxelPosition.z + dz });

					// Index of the node local to the element
					size_t nodeLocalIndex = (dx + (dy << 1) + (dz << 2));
					// Index of the element local to the node 
					size_t elementLocalIndex = (FEMesh::element_type::numNodes - 1) - nodeLocalIndex;

					if (found_node == nodeMap.end()) {

						//Create a new node
						FEMesh::node_type n;
						n.x = voxelPosition.x + dx;
						n.y = voxelPosition.y + dy;
						n.z = voxelPosition.z + dz;
						n.idx = nodeCount;
						n.elements = { {-1, -1, -1, -1, -1, -1, -1, -1} };

						// Update the current node and element to include this each other respectively in their adjacency info
						n.elements[elementLocalIndex] = elementCount; // std::array<uint32_t, NodesPerElement>
						e.nodes[nodeLocalIndex] = nodeCount;

						// Add the nodes position to the map
						nodeMap.insert(found_node, std::make_pair(Position{ voxelPosition.x + dx, voxelPosition.y + dy, voxelPosition.z + dz }, nodeCount));

						// Add the node to the mesh
						feMesh.nodes.emplace_back(std::move(n));
						nodeCount++;
					}
					else {
						auto& n = feMesh.nodes[found_node->second];

						n.elements[elementLocalIndex] = e.idx;
						e.nodes[nodeLocalIndex] = n.idx;
					}

				}

				feMesh.elements.emplace_back(std::move(e));
				elementCount++;
			}
		}
		
	}

	return feMesh;
}