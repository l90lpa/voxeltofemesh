#pragma once

#include "voxelGrid/VoxelGrid.h"
#include "FEMesh.h"

FEMesh voxToFEMesh(const VoxelGrid<BitGrid3<uint32_t, uint32_t>, float>& voxelGrid);