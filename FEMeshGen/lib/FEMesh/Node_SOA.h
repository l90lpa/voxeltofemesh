
#pragma once

#include <cstdint>
#include <vector>

#include <glm/vec3.hpp>

#include "EigenTypes.h"
#include "FEMesh/Node.h"


template<typename FP_T, uint8_t NodesPerElement, Eigen::StorageOptions StorageLayout>
class Node_SOA {
public:
	VectorCol<uint32_t> ids;
	Eigen::Matrix<FP_T, Eigen::Dynamic, 3, StorageLayout> positions;

	// Topology
	Eigen::Matrix<int32_t, Eigen::Dynamic, NodesPerElement, StorageLayout> elements;

	Node_SOA() = default;

	Node_SOA(uint32_t numElements) {
		ids.resize(numElements, Eigen::NoChange);
		positions.resize(numElements, Eigen::NoChange);
		elements.resize(numElements, Eigen::NoChange);
	}

	bool empty() const {
		return is_empty(ids) && is_empty(positions) && is_empty(elements);
	}

	tl::expected<size_t, std::string> size() const {
		// Check that all the members have consistent dimensions

		auto testValue = ids.rows();

		bool are_consistent = (testValue == positions.rows());
		are_consistent = (are_consistent && (testValue == elements.rows()));

		if (are_consistent)
			return testValue;
		else
			return tl::make_unexpected(std::string{ "Inconsistent member sizes." });
	}
};

template<typename FP_T, uint8_t NodesPerElement, Eigen::StorageOptions StorageLayout>
Node<FP_T, NodesPerElement> getNode(const size_t index, const Node_SOA<FP_T, NodesPerElement, StorageLayout>& nodes) {

	Node<FP_T, NodesPerElement> node;

	node.id = nodes.ids(index);

	for (size_t j = 0; j < nodes.positions.cols(); ++j) {
		node.position[j] = nodes.positions(index, j);
	}

	for (size_t j = 0; j < nodes.elements.cols(); ++j) {
		node.elements[j] = nodes.elements(index, j);
	}

	return node;
}