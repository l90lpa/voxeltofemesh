
#pragma once

#include <cstdint>
#include <string>
#include <vector>

#include <tl/expected.hpp>

#include "EigenTypes.h"
#include "FEMesh/Element.h"


template<typename FP_T, uint8_t NodesPerElement, Eigen::StorageOptions StorageLayout>
class Elements_SOA {
public:
	Eigen::Matrix<FP_T, Eigen::Dynamic, 3, StorageLayout> positions;
	VectorCol<uint32_t> ids;
	VectorCol<uint32_t> materials;
	Eigen::Matrix<FP_T, Eigen::Dynamic, NodesPerElement, StorageLayout> nodes;

	Elements_SOA() = default;

	Elements_SOA(uint32_t numElements) {
		positions.resize(numElements, Eigen::NoChange);
		ids.resize(numElements, Eigen::NoChange);
		materials.resize(numElements, Eigen::NoChange);
		nodes.resize(numElements, Eigen::NoChange);
	}

	bool empty() const {
		return is_empty(ids) && is_empty(positions) && is_empty(materials) && is_empty(nodes);
	}

	tl::expected<size_t, std::string> size() const {
		// Check that all the members have consistent dimensions

		auto testValue = ids.rows();

		bool are_consistent = (testValue == positions.rows());
		are_consistent = (are_consistent && (testValue == materials.rows()));
		are_consistent = (are_consistent && (testValue == nodes.rows()));

		if (are_consistent)
			return testValue;
		else
			return tl::make_unexpected(std::string{ "Inconsistent member sizes." });
	}
};

template<typename FP_T, Eigen::StorageOptions StorageLayout>
Element<FP_T> getElement(const size_t index, const Elements_SOA<FP_T, StorageLayout>& elements) {

	Element<FP_T> element;

	element.x = elements.positions(index, 0);
	element.y = elements.positions(index, 1);
	element.z = elements.positions(index, 2);

	element.idx = elements.ids(index);
	element.material = elements.materials(index);

	for (size_t j = 0; j < elements.nodes.cols(); ++j) {
		element.nodes[j] = elements.nodes(index, j);
	}

	return element;
}