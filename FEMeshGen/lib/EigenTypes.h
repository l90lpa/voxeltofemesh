
#pragma once 

#include <Eigen/Dense>

template<typename Element_T>
using VectorCol = Eigen::Matrix<Element_T, Eigen::Dynamic, 1>;

template<typename Element_T, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols>
bool is_empty(const Eigen::Matrix<Element_T, _Rows, _Cols, _Options, _MaxRows, _MaxCols>& matrix) {
	return matrix.cols() == 0 && matrix.rows() == 0;
}