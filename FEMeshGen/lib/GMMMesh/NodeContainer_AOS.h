
#pragma once

#include <vector>

#include "GMMMesh/Node.h"

namespace gmm {

	template<typename FP_T>
	using LeafNodeContainer_AOS = std::vector<LeafNode<FP_T>>;


	template<typename FP_T>
	using BranchNodeContainer_AOS = std::vector<BranchNode<FP_T>>;

} // namespace gmm
