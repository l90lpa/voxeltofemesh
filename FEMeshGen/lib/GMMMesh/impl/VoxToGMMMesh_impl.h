
#pragma once

#include "GMMMesh/GMMMesh_AOS.h"
#include "MeshCommon.h"
#include "voxelGrid/Mipmap.h"
#include "voxelGrid/VoxelGrid.h"
#include "Tools/SpatialHash.h"
#include "Tools/SpatialComparators.h"

#include <algorithm>

#include <glm/gtx/component_wise.hpp>
#include <gsl-lite.hpp>

namespace gmm {
	namespace impl {
		
		// Overview:
		// Effectively a bidirectional map between grid position of a node and index of a node in the LeafLevel
		// or BranchLevel.
		//
		// Members:
		// - nodeGridPosition: contains the grid position for each node in LeafLevel or BranchLevel in the same 
		//   order.
		// - gridToIndex: map from grid position to index for a given node in the LeafLevel or BranchLevel
		//
		template<typename FP_t, typename Node_Index_t>
		struct NodeConstructionData{
			std::vector<glm::tvec3<Node_Index_t>> nodeGridPositions;
			SpatialHashTable_r3<FP_t, Node_Index_t> gridToIndex;
		};

		// Overview:
		// construct the nodes and elements of the leaf level, sorting the nodes in memory such that they are
		// striped across the domain.
		//
		// We update the connectivity node-element and element-node but not node-node 
		//
		template<typename FP_t, typename Node_Index_t, typename BitGrid_T, typename FP_Coord_T>
		std::pair<LeafLevel<FP_t>, SpatialHashTable_r3<Node_Index_t, Node_Index_t>>
			constructSimulationLevelNodes(const VoxelGrid<BitGrid_T, FP_Coord_T>& vGrid) {
			
			using VGrid = VoxelGrid<BitGrid_T, FP_Coord_T>;

			// build the spatial hash and the node list
			// sort the node list
			// update the spatial hash
			// build the element list adding the connectivity between nodes and elements

			LeafLevel<FP_t> leafLevel;
			SpatialHashTable_r3<Node_Index_t, Node_Index_t> nodeMap;

			{
				nodeMap.reserve(0.65 * vGrid.numVoxels()); // TODO: <<< change this to

				Node_Index_t nodeCount = 0;

				auto it = vGrid.begin();
				auto end = vGrid.end();
				for (; it != end; ++it) {
					if (*it == ElementState::Active) {
						const auto elementGridPosition = (*it).gridPosition();
						const auto elementWorldPosition = (*it).worldPosition();
						// so we've found a voxel:
						// - lets create 8 nodes
						{
							for (size_t k = 0; k < 8; ++k) {

								// Check if the node has already been created
								Node_Index_t int_dx = (k % 2);
								Node_Index_t int_dy = ((k / 2) % 2);
								Node_Index_t int_dz = (k / 4);
								glm::tvec3<Node_Index_t> gridOffset{ int_dx, int_dy, int_dz };
								auto nodeGridPosition = elementGridPosition + gridOffset;

								if (auto found_node = nodeMap.find(nodeGridPosition);  found_node == nodeMap.end()) {

									auto worldOffset = static_cast<glm::tvec3<FP_t>>(gridOffset) * vGrid.unitVoxelDims_;
									auto nodeWorldPosition = elementWorldPosition + worldOffset;


									//Create a new node
									LeafLevel<FP_t>::node_type n;
									n.id = nodeCount;
									n.gridPosition = nodeGridPosition;
									n.initialPosition = nodeWorldPosition;
									std::fill(n.elements.begin(), n.elements.end(), mcommon::EmptyIndex);
									std::fill(n.adjNodes.begin(), n.adjNodes.end(), mcommon::EmptyIndex);
									std::fill(n.coarseNodes.begin(), n.coarseNodes.end(), mcommon::EmptyIndex);

									// Add the nodes position to the map
									nodeMap.insert(found_node, std::make_pair(nodeGridPosition, nodeCount));

									// Add the node to the mesh
									leafLevel.nodes.emplace_back(std::move(n));
									nodeCount++;
								}
							}
						}
					}
				}
			}
			
			// Determine the subset that the node belongs for parallel (multi-colour) Gauss-Seidel method
			//- compute the set of 'a' and 'b'. Find the global location and take modulo 8 of this to get the set 
			//- compare the sets
			//- order within the set
			std::sort(leafLevel.nodes.begin(), leafLevel.nodes.end(), [&](const auto & node_a, const auto & node_b) {

				const size_t setA = (node_a.gridPosition.x % 2) | ((node_a.gridPosition.y % 2) << 1) | 
					((node_a.gridPosition.z % 2) << 2);
				const size_t setB = (node_b.gridPosition.x % 2) | ((node_b.gridPosition.y % 2) << 1) |
					((node_b.gridPosition.z % 2) << 2);
				if (setA < setB) {
					return true;
				}
				else if(setA == setB) {
					return lessThan_lexicographically(node_a.gridPosition, node_b.gridPosition);
				}
				else {
					return false;
				}
			});

			for (Node_Index_t i = 0; i < leafLevel.nodes.size(); ++i) {
				leafLevel.nodes[i].id = i;
				nodeMap[leafLevel.nodes[i].gridPosition] = leafLevel.nodes[i].id;
			}

			{
				Node_Index_t elementCount = 0;

				auto it = vGrid.begin();
				auto end = vGrid.end();
				for (; it != end; ++it) {
				
					if (*it == ElementState::Active) {

						const auto elementGridPosition = (*it).gridPosition();
						// so we've found a voxel:
						// - lets create 8 nodes

						{
							LeafLevel<FP_t>::element_type e;
							e.id = elementCount;
							std::fill(e.nodes.begin(), e.nodes.end(), mcommon::EmptyIndex);


							for (size_t k = 0; k < 8; ++k) {

								// Check if the node has already been created
								Node_Index_t int_dx = (k % 2);
								Node_Index_t int_dy = ((k / 2) % 2);
								Node_Index_t int_dz = (k / 4);
								glm::tvec3<Node_Index_t> gridOffset{int_dx, int_dy, int_dz};

								auto iteratorToAdjacentNode = nodeMap.find(elementGridPosition + gridOffset);
								Expects(iteratorToAdjacentNode != nodeMap.end());

								auto [nodePosition, nodeIndex] = *iteratorToAdjacentNode;

								// Index of the node local to the element
								size_t nodeLocalIndex = (int_dx + (int_dy << 1) + (int_dz << 2));
								// Index of the element local to the node (using 7 as it is the last local index)
								size_t elementLocalIndex = 7 - nodeLocalIndex;

								leafLevel.nodes[nodeIndex].elements[elementLocalIndex] = elementCount;
								e.nodes[nodeLocalIndex] = nodeIndex;
							}
							leafLevel.elements.emplace_back(std::move(e));
							++elementCount;
						}
					}
				
				}
			}
			
			Ensures(leafLevel.nodes.size() == nodeMap.size());
			return std::make_pair(std::move(leafLevel), std::move(nodeMap));
		}

		template<typename FP_t, typename Node_Index_t, typename BitGrid_T, typename FP_Coord_T>
		std::pair<BranchLevel<FP_t>, SpatialHashTable_r3<Node_Index_t, Node_Index_t>>
			constructCoarseLevelNodes(const VoxelGrid<BitGrid_T, FP_Coord_T>& vGrid) {
	
			using VGrid = VoxelGrid<BitGrid_T, FP_Coord_T>;

			// build the spatial hash and the node list
			// sort the node list
			// update the spatial hash
			// build the element list adding the connectivity between nodes and elements

			BranchLevel<FP_t> branchLevel;
			SpatialHashTable_r3<Node_Index_t, Node_Index_t> nodeMap;

			{
				nodeMap.reserve(0.65 * vGrid.numVoxels()); // TODO: <<< change this to

				Node_Index_t nodeCount = 0;

				auto it = vGrid.begin();
				auto end = vGrid.end();
				for (; it != end; ++it) {
					if (*it == ElementState::Active) {
						const auto elementGridPosition = (*it).gridPosition();
						// so we've found a voxel:
						// - lets create 8 nodes
						{
							for (size_t k = 0; k < 8; ++k) {

								// Check if the node has already been created
								Node_Index_t int_dx = (k % 2);
								Node_Index_t int_dy = ((k / 2) % 2);
								Node_Index_t int_dz = (k / 4);
								glm::tvec3<Node_Index_t> gridOffset{ int_dx, int_dy, int_dz };
								auto nodeGridPosition = elementGridPosition + gridOffset;

								if (auto found_node = nodeMap.find(nodeGridPosition);  found_node == nodeMap.end()) {

									//Create a new node
									BranchLevel<FP_t>::node_type n;
									n.id = nodeCount;
									n.gridPosition = nodeGridPosition;
									std::fill(n.adjNodes.begin(), n.adjNodes.end(), mcommon::EmptyIndex);
									std::fill(n.fineNodes.begin(), n.fineNodes.end(), mcommon::EmptyIndex);
									std::fill(n.coarseNodes.begin(), n.coarseNodes.end(), mcommon::EmptyIndex);


									// Add the nodes position to the map
									nodeMap.insert(found_node, std::make_pair(nodeGridPosition, nodeCount));

									// Add the node to the mesh
									branchLevel.nodes.emplace_back(std::move(n));
									nodeCount++;
								}
							}
						}
					}
				}
			}

			// Determine the subset that the node belongs for parallel (multi-colour) Gauss-Seidel method
			//- compute the set of 'a' and 'b'. Find the global location and take modulo 8 of this to get the set 
			//- compare the sets
			//- order within the set
			std::sort(branchLevel.nodes.begin(), branchLevel.nodes.end(), [&](const auto& node_a, const auto& node_b) {

				const size_t setA = (node_a.gridPosition.x % 2) | ((node_a.gridPosition.y % 2) << 1) | ((node_a.gridPosition.z % 2) << 2);
				const size_t setB = (node_b.gridPosition.x % 2) | ((node_b.gridPosition.y % 2) << 1) | ((node_b.gridPosition.z % 2) << 2);
				if (setA < setB) {
					return true;
				}
				else if (setA == setB) {
					return lessThan_lexicographically(node_a.gridPosition, node_b.gridPosition);
				}
				else {
					return false;
				}
				});

			for (Node_Index_t i = 0; i < branchLevel.nodes.size(); ++i) {
				branchLevel.nodes[i].id = i;
				nodeMap[branchLevel.nodes[i].gridPosition] = branchLevel.nodes[i].id;
			}

			Ensures(branchLevel.nodes.size() == nodeMap.size());
			return std::make_pair(std::move(branchLevel), std::move(nodeMap));

		}

		// Overview:
		// For the leaf level nodes update the adjacent nodes information
		//
		template<typename FP_t, typename Index_t>
		void 
			updateLevelTopology(LeafLevel<FP_t>& leafLevel, 
				const SpatialHashTable_r3<Index_t, Index_t>& nodeConstructionData) {

			Expects(leafLevel.nodes.size() == nodeConstructionData.size());
			// for each node in the leaf level 
			// - check if any of the surrounding nodes exist and update the connectivity information (adjacentNodes)

			for (auto& currNode : leafLevel.nodes) {

				int32_t index = 0;
				bool firstHalf = true;
				for (int32_t k = -1; k < 2; ++k) {
					for (int32_t j = -1; j < 2; ++j) {
						for (int32_t i = -1; i < 2; ++i, ++index) {
							if (firstHalf && index == 13) {
								index -= 1;
								firstHalf = false;
								continue;
							}
							glm::tvec3<int32_t> direction{ i, j, k };

							// Computation is based on the adjacent element indices of a given node being stored 
							// lexicographically (x,y,z) and vice versa for a given element. 7 is used as this is
							// max local element index. 
							// We use the relationship that an adjacent element index of a given node is related
							// to the adjacent node index of the element by the relation:
							//   nodeIndex == (7 - elementIndex)
							// when they are stored lexicographically.
							const uint32_t localElementIndex = glm::compAdd(
								glm::clamp(static_cast<glm::uvec3>(direction + glm::ivec3{1,1,1}), glm::uvec3{ 0,0,0 }, glm::uvec3{ 1,1,1 }) *
								glm::uvec3{ 1,2,4 });
							const uint32_t localNodeIndex = (7 - localElementIndex) + glm::compAdd(direction * glm::ivec3{ 1,2,4 });

							const int32_t elementIndex = currNode.elements[localElementIndex];
							if (elementIndex != mcommon::EmptyIndex) {
								const int32_t nodeIndex = leafLevel.elements[elementIndex].nodes[localNodeIndex];

								currNode.adjNodes[index] = nodeIndex;
							}
						}
					}
				}
			}
		}

		// Overview:
		// For the branch level nodes update the adjacent nodes information
		//
		template<typename FP_t, typename Index_t>
		void 
			updateLevelTopology(BranchLevel<FP_t>& branchLevel, 
				const SpatialHashTable_r3<Index_t, Index_t>& nodeConstructionData) {

			Expects(branchLevel.nodes.size() == nodeConstructionData.size());

			 // for each node in the branch level 
			 // - check if any of the surrounding nodes exist and update the connectivity information (adjacentNodes)

			for (auto& currNode : branchLevel.nodes) {

				int32_t index = 0;
				bool firstHalf = true;

				for (int32_t k = -1; k < 2; ++k) {
					for (int32_t j = -1; j < 2; ++j) {
						for (int32_t i = -1; i < 2; ++i, ++index) {
							
							if (firstHalf && index == 13) {
								index -= 1;
								firstHalf = false;
								continue;
							}
							
							glm::tvec3<int32_t> direction{ i, j, k };
							auto adjNodePosition = static_cast<glm::tvec3<int32_t>>(currNode.gridPosition) + direction;

							if (glm::all(glm::greaterThanEqual(adjNodePosition, glm::ivec3{0,0,0}))) {
								if (auto result = nodeConstructionData.find(adjNodePosition); result != nodeConstructionData.end()) {
									currNode.adjNodes[index] = result->second;
								}
							}
						}
					}
				}
			}
		}

		// Overview:
		// For the leaf level nodes and a set of branch level nodes update the finer/coarser nodes information
		//
		template<typename FP_t, typename Index_t>
		void 
			updateInterLevelTopology(LeafLevel<FP_t>& leafLevel, 
				const SpatialHashTable_r3<Index_t, Index_t>& simLevel_nodeConstructionData,
				BranchLevel<FP_t>& branchLevel, const SpatialHashTable_r3<Index_t, Index_t>& branchLevel_nodeConstructionData) {

			Expects(leafLevel.nodes.size() == simLevel_nodeConstructionData.size());
			Expects(branchLevel.nodes.size() == branchLevel_nodeConstructionData.size());

			// This is a map from the lessThan_lexicographically position of a finer node (about a coarse node) to the 
			// lessThan_lexicographically position of the coarse node in the fine nodes `coarseNodes` array 
			static const std::vector<uint32_t> coarseNodesPosition{ 7, 3, 6, 3, 1, 2, 5, 2, 4, 3, 1, 2, 1, 0, 0, 1, 0, 0, 3, 1, 2, 1, 0, 0, 1, 0, 0 };

			// Scale to grid position of the coarse grid nodes
			// for each coarse grid node:
			// - loop through the adjacent finer nodes
			//   - update the coarser nodes connectivity with the finer node (`finerNodes`)
			//   - for each finer node:
			//     - compute its 'type'/'location' {vertex, edge, plane, cell}
			//     - compute the relative location of the coarse node from the fine node and then its position in, 
			//       `coarseNodes`
			//     - update the finer nodes connectivity with the coareser node (`coaserNodes`)

			size_t coarseNodeIndex = 0;
			for (auto& coarseNode : branchLevel.nodes) {
				
				const auto coarseNodeScaledGridPosition = 2 * coarseNode.gridPosition;
				size_t lutIndex = 0;
				for (int32_t k = -1; k < 2; ++k) {
					for (int32_t j = -1; j < 2; ++j) {
						for (int32_t i = -1; i < 2; ++i) {
							glm::ivec3 direction{ i, j, k };

							const auto fineNodeGridPosition = static_cast<glm::ivec3>(coarseNodeScaledGridPosition) + direction;

							// Make sure we aren't about to check a position outside of the domain.
							if (glm::all(glm::greaterThanEqual(fineNodeGridPosition, glm::ivec3{ 0,0,0 }))) {
								const auto fineNodeIndex = simLevel_nodeConstructionData.find(static_cast<glm::uvec3>(fineNodeGridPosition));

								if (fineNodeIndex != simLevel_nodeConstructionData.end()) {
									coarseNode.fineNodes[lutIndex] = fineNodeIndex->second;
									leafLevel.nodes[fineNodeIndex->second].coarseNodes[coarseNodesPosition[lutIndex]] = coarseNodeIndex;
								}
							}
							++lutIndex;
						}
					}
				}
				++coarseNodeIndex;
			}
		}

		// Overview:
		// For 2 sets of branch level nodes update the finer/coarser nodes information
		//
		template<typename FP_t, typename Index_t>
		void 
			updateInterLevelTopology(BranchLevel<FP_t>& branchLv_a, 
				const SpatialHashTable_r3<Index_t, Index_t>& branchLv_a_nodeConstructionData,
				BranchLevel<FP_t>& branchLv_b, const SpatialHashTable_r3<Index_t, Index_t>& branchLv_b_nodeConstructionData) {

			Expects(branchLv_a.nodes.size() == branchLv_a_nodeConstructionData.size());
			Expects(branchLv_b.nodes.size() == branchLv_b_nodeConstructionData.size());

			// This is a map from the lessThan_lexicographically position of a finer node (about a coarse node) to the 
			// lessThan_lexicographically position of the coarse node in the fine nodes `coarseNodes` array 
			static const std::vector<uint32_t> coarseNodesPosition{ 7, 3, 6, 3, 1, 2, 5, 2, 4, 3, 1, 2, 1, 0, 0, 1, 0, 0, 3, 1, 2, 1, 0, 0, 1, 0, 0 };

			// Scale to grid position of the coarse grid nodes
			// for each coarse grid node:
			// - loop through the adjacent finer nodes
			//   - update the coarser nodes connectivity with the finer node (`finerNodes`)
			//   - for each finer node:
			//     - compute its 'type'/'location' {vertex, edge, plane, cell}
			//     - compute the relative location of the coarse node from the fine node and then its position in, 
			//       `coarseNodes`
			//     - update the finer nodes connectivity with the coareser node (`coaserNodes`)

			size_t coarseNodeIndex = 0;
			for (auto& coarseNode : branchLv_b.nodes) {

				const auto coarseNodeScaledGridPosition = 2 * coarseNode.gridPosition;
				size_t lutIndex = 0;
				for (int32_t k = -1; k < 2; ++k) {
					for (int32_t j = -1; j < 2; ++j) {
						for (int32_t i = -1; i < 2; ++i) {
							glm::ivec3 direction{ i, j, k };

							const auto fineNodeGridPosition = static_cast<glm::ivec3>(coarseNodeScaledGridPosition) + direction;

							// Make sure we aren't about to check a position outside of the domain.
							if (glm::all(glm::greaterThanEqual(fineNodeGridPosition, glm::ivec3{ 0,0,0 }))) {
								const auto fineNodeIndex = branchLv_a_nodeConstructionData.find(static_cast<glm::uvec3>(fineNodeGridPosition));

								if (fineNodeIndex != branchLv_a_nodeConstructionData.end()) {
									coarseNode.fineNodes[lutIndex] = fineNodeIndex->second;
									branchLv_a.nodes[fineNodeIndex->second].coarseNodes[coarseNodesPosition[lutIndex]] = coarseNodeIndex;
								}
							}
							++lutIndex;
						}
					}
				}
				++coarseNodeIndex;
			}

		}

	} // namespace impl
} // namespace gmm