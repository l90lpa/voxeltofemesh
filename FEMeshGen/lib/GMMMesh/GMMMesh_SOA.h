
#pragma once

#include "GMMMesh/ElementContainer_SOA.h"
#include "GMMMesh/NodeContainer_SOA.h"


namespace gmm {

	template<typename FP_t, Eigen::StorageOptions StorageLayout = Eigen::RowMajor>
	struct LeafLevel_SOA {
		using node_type = typename LeafNodeContainer_SOA<FP_t, StorageLayout>::value_type;
		using element_type = typename ElementContainer_SOA<FP_t, StorageLayout>::value_type;

		ElementContainer_SOA<FP_t, StorageLayout> elements;
		LeafNodeContainer_SOA<FP_t, StorageLayout> nodes;

		bool empty() const {
			return elements.empty() && nodes.empty();
		}
	};

	template<typename FP_t, Eigen::StorageOptions StorageLayout = Eigen::RowMajor>
	struct BranchLevel_SOA {
		using node_type = typename BranchNodeContainer_SOA<FP_t, StorageLayout>::value_type;

		BranchNodeContainer_SOA<FP_t, StorageLayout> nodes;

		bool empty() const {
			return nodes.empty();
		}
	};

	template<typename FP_t, Eigen::StorageOptions StorageLayout = Eigen::RowMajor>
	class GMMMesh_SOA {
	public:
		LeafLevel_SOA<FP_t, StorageLayout> leafLevel;
		std::vector<BranchLevel_SOA<FP_t, StorageLayout>> branchLevels;

		bool empty() const {
			return leafLevel.empty() && branchLevels.empty();
		}
	};

} // namespace gmm