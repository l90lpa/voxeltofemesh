
#pragma once

#include <cstdint>
#include <string>
#include <vector>

#include <tl/expected.hpp>

#include "EigenTypes.h"
#include "GMMMesh/Element.h"


namespace gmm {



	template<typename FP_T>
	class ElementsICsContainer_SOA {
	public:
		VectorCol<FP_T> elasticModulus;
		VectorCol<FP_T> density;

		bool empty() const {
			return is_empty(elasticModulus) && is_empty(density);
		}
	};

	template<typename FP_T, Eigen::StorageOptions StorageLayout>
	class ElementContainer_SOA {
	public:
		using value_type = Element<FP_T>;

		VectorCol<uint32_t> ids;
		Eigen::Matrix<int32_t, Eigen::Dynamic, 8, StorageLayout> nodes;

		ElementsICsContainer_SOA<FP_T> ICs;

		ElementContainer_SOA() = default;

		ElementContainer_SOA(uint32_t numElements) {
			ids.resize( numElements, Eigen::NoChange );
			nodes.resize(numElements, Eigen::NoChange);
			ICs.elasticModulus.resize(numElements, Eigen::NoChange);
			ICs.density.resize(numElements, Eigen::NoChange);
		}

		bool empty() const {
			return is_empty(ids) && is_empty(nodes) && ICs.empty();
		}

		tl::expected<size_t, std::string> size() const {
			// Check that all the members have consistent dimensions

			auto testValue = ids.rows();

			bool are_consistent = (testValue == nodes.rows());
			are_consistent = (are_consistent && (testValue == ICs.density.rows()));
			are_consistent = (are_consistent && (testValue == ICs.elasticModulus.rows()));

			if (are_consistent)
				return testValue;
			else
				return tl::make_unexpected(std::string{ "Inconsistent member sizes." });
		}
	};

	template<typename FP_T, Eigen::StorageOptions StorageLayout>
	Element<FP_T> getElement(const size_t index, const ElementContainer_SOA<FP_T, StorageLayout>& elements) {

		Element<FP_T> element;

		element.id = elements.ids(index);
		
		for (size_t j = 0; j < elements.nodes.cols(); ++j) {
			element.nodes[j] = elements.nodes(index, j);
		}

		element.initialConditions.elasticModulus = elements.ICs.elasticModulus(index);
		element.initialConditions.density = elements.ICs.density(index);

		return element;
	}

} // namespace gmm