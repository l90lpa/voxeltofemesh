
#pragma once

#include <type_traits>
#include <vector>

#include "GMMMesh/ElementContainer_AOS.h"
#include "GMMMesh/NodeContainer_AOS.h"
#include "ElementTypes.h"

namespace gmm {

	template<typename FP_t>
	struct LeafLevel {
		typedef typename LeafNodeContainer_AOS<FP_t>::value_type node_type;
		typedef typename ElementContainer_AOS<FP_t>::value_type element_type;

		LeafNodeContainer_AOS<FP_t> nodes;
		ElementContainer_AOS<FP_t> elements;
	};

	template<typename FP_t>
	struct BranchLevel {
		typedef typename BranchNodeContainer_AOS<FP_t>::value_type node_type;

		BranchNodeContainer_AOS<FP_t> nodes;
	};

	template<typename FP_t>
	class GMMMesh {
	public:
		LeafLevel<FP_t> leafLevel;
		std::vector<BranchLevel<FP_t>> branchLevels;
	};

} // namespace gmm