
#pragma once

#include <algorithm>
#include <optional>

#include "GMMMesh_AOS.h"
#include "Tools/FiniteSpaceTools.h"

namespace gmm {

	enum class CheckResult {
		Correct,
		Incorrect
	};

	template<template<typename> class Level_t, typename FP_t>
	CheckResult intraLevelTopologyCheck(const Level_t<FP_t>& levelMesh) {
		
		static constexpr glm::uvec3 localSpaceDimensions{3,3,3};
		static const uint32_t adjNodesAdjustmentIndex = glm::compMul(localSpaceDimensions) / 2;

		size_t count = 0;
		for (const auto& node : levelMesh.nodes) {
			Expects(count == node.id);

			// check if all of its adjacent nodes map back to it
			for (const auto& adjNodeID : node.adjNodes) {
				
				if (adjNodeID == mcommon::EmptyIndex) {
					continue;
				}
				
				const auto& adjNode = levelMesh.nodes[adjNodeID];

				auto localIndex = positionToLocalizedIndex(node.gridPosition, adjNode.gridPosition, localSpaceDimensions, SpatialOrdering::LexicographicXYZTag{});

				// adjacent nodes index adjustment
				if (localIndex > adjNodesAdjustmentIndex) {
					localIndex -= 1;
				}

				if (count != adjNode.adjNodes[localIndex]) {
					return CheckResult::Incorrect;
				}
			}
			++count;
		}

		return CheckResult::Correct;
	}

	template<template<typename> class LevelA_t, template<typename> class LevelB_t, typename FP_t>
	CheckResult coarseToFineTopologyCheck(const LevelA_t<FP_t>& branchMesh_a, const LevelB_t<FP_t>& branchMesh_b) {
		// for each branch node check if the finer nodes map back to it

		for (const auto& coarseNode : branchMesh_b.nodes) {
			for (const auto& fineNodeID : coarseNode.fineNodes) {

				if (fineNodeID == mcommon::EmptyIndex) {
					continue;
				}

				const auto& fineNode = branchMesh_a.nodes[fineNodeID];

				// check if the branch node is present
				auto result = std::find(fineNode.coarseNodes.begin(), fineNode.coarseNodes.end(), coarseNode.id);
				if (result == fineNode.coarseNodes.end()) {
					return CheckResult::Incorrect;
				}
			}
		}
		return CheckResult::Correct;
	}

	template<template<typename> class LevelA_t, template<typename> class LevelB_t, typename FP_t>
	CheckResult fineToCoarseTopologyCheck(const LevelA_t<FP_t>& branchMesh_a, const LevelB_t<FP_t>& branchMesh_b) {
		// for each fine node check if the branch nodes map back to it

		for (const auto& fineNode : branchMesh_a.nodes) {
			for (const auto& coarseNodeID : fineNode.coarseNodes) {

				if (coarseNodeID == mcommon::EmptyIndex) {
					continue;
				}

				const auto& coarseNode = branchMesh_b.nodes[coarseNodeID];

				// check if the fine node is present
				auto result = std::find(coarseNode.fineNodes.begin(), coarseNode.fineNodes.end(), fineNode.id);
				if (result == coarseNode.fineNodes.end()) {
					return CheckResult::Incorrect;
				}
			}
		}
		return CheckResult::Correct;
	}

	template<template<typename> class LevelA_t, template<typename> class LevelB_t, typename FP_t>
	CheckResult interLevelTopologyCheck(const LevelA_t<FP_t>& branchMesh_a, const LevelB_t<FP_t>& branchMesh_b) {
		CheckResult resultA = coarseToFineTopologyCheck(branchMesh_a, branchMesh_b);
		CheckResult resultB = fineToCoarseTopologyCheck(branchMesh_a, branchMesh_b);

		CheckResult result = (resultA == CheckResult::Incorrect || resultB == CheckResult::Incorrect) ? 
			CheckResult::Incorrect : CheckResult::Correct;

		return result;
	}

	template<typename FP_t>
	CheckResult meshValidator(const GMMMesh<FP_t>& gmmMesh) {
		// for the mesh to be valid:
		// - check that the node indices are within range
		// - check that connectivity is bidirectional
		// - check the proximity of nodes
		// - check the ordering of nodes within a level
		// - check the ordering of nodes within the connectivity data

		auto leafLevel_intraTopologyResult = intraLevelTopologyCheck(gmmMesh.leafLevel);

		// early exit
		if (leafLevel_intraTopologyResult == CheckResult::Incorrect) {
			return CheckResult::Incorrect;
		}

		if (!gmmMesh.branchLevels.empty()) {

			auto branchLevel_intraTopologyResult = intraLevelTopologyCheck(gmmMesh.branchLevels.front());

			if (branchLevel_intraTopologyResult == CheckResult::Incorrect) {
				return CheckResult::Incorrect;
			}
			
			auto interTopologyResult = interLevelTopologyCheck(gmmMesh.leafLevel, gmmMesh.branchLevels.front());

			if (interTopologyResult == CheckResult::Incorrect) {
				return CheckResult::Incorrect;
			}

			for (size_t i = 1; i < gmmMesh.branchLevels.size(); ++i) {
				branchLevel_intraTopologyResult = intraLevelTopologyCheck(gmmMesh.branchLevels[i]);

				if (branchLevel_intraTopologyResult == CheckResult::Incorrect) {
					return CheckResult::Incorrect;
				}

				interTopologyResult = interLevelTopologyCheck(gmmMesh.branchLevels[i - 1], gmmMesh.branchLevels[i]);

				if (interTopologyResult == CheckResult::Incorrect) {
					return CheckResult::Incorrect;
				}
			}
		}

		return CheckResult::Correct;
	}
}