
#pragma once

#include <gsl-lite.hpp>

#include "GMMMesh/GMMMesh_AOS.h"
#include "GMMMesh/GMMMesh_SOA.h"


namespace gmm {

	template<typename FP_T, Eigen::StorageOptions storageLayout>
	ElementContainer_SOA<FP_T, storageLayout> to_SOA(const ElementContainer_AOS<FP_T>& elementsAOS) {

		ElementContainer_SOA<FP_T, storageLayout> elementsSOA{ gsl::narrow_cast<uint32_t>( elementsAOS.size() ) };
		
		const size_t numElements = elementsAOS.size();

		for (size_t i = 0; i < numElements; ++i) {
			elementsSOA.ids(i) = elementsAOS[i].id;

			for (size_t j = 0; j < elementsAOS[i].nodes.size(); ++j) {
				elementsSOA.nodes(i, j) = elementsAOS[i].nodes[j];
			}

			elementsSOA.ICs.elasticModulus(i) = elementsAOS[i].initialConditions.elasticModulus;
			elementsSOA.ICs.density(i) = elementsAOS[i].initialConditions.density;
		}

		return elementsSOA;
	}

	template<typename FP_T, Eigen::StorageOptions storageLayout>
	LeafNodeContainer_SOA<FP_T, storageLayout> to_SOA(const LeafNodeContainer_AOS<FP_T>& nodesAOS) {

		LeafNodeContainer_SOA<FP_T, storageLayout> nodesSOA{ gsl::narrow_cast<uint32_t>( nodesAOS.size() ) };

		const size_t numNodes = nodesAOS.size();

		for (size_t i = 0; i < numNodes; ++i) {
			nodesSOA.ids(i) = nodesAOS[i].id;

			nodesSOA.gridPositions(i, 0) = nodesAOS[i].gridPosition.x;
			nodesSOA.gridPositions(i, 1) = nodesAOS[i].gridPosition.y;
			nodesSOA.gridPositions(i, 2) = nodesAOS[i].gridPosition.z;

			for (size_t j = 0; j < nodesAOS[i].elements.size(); ++j) {
				nodesSOA.elements(i, j) = nodesAOS[i].elements[j];
			}

			for (size_t j = 0; j < nodesAOS[i].adjNodes.size(); ++j) {
				nodesSOA.adjNodes(i, j) = nodesAOS[i].adjNodes[j];
			}

			for (size_t j = 0; j < nodesAOS[i].coarseNodes.size(); ++j) {
				nodesSOA.coarseNodes(i, j) = nodesAOS[i].coarseNodes[j];
			}

			nodesSOA.initialPositions(i, 0) = nodesAOS[i].initialPosition.x;
			nodesSOA.initialPositions(i, 1) = nodesAOS[i].initialPosition.y;
			nodesSOA.initialPositions(i, 2) = nodesAOS[i].initialPosition.z;

			nodesSOA.initialConditions.initialForces(i, 0) = nodesAOS[i].initialConditions.initialForce.x;
			nodesSOA.initialConditions.initialForces(i, 1) = nodesAOS[i].initialConditions.initialForce.y;
			nodesSOA.initialConditions.initialForces(i, 2) = nodesAOS[i].initialConditions.initialForce.z;

			nodesSOA.initialConditions.is_fixedPositions(i) = nodesAOS[i].initialConditions.is_fixedPosition;
		}

		return nodesSOA;
	}

	template<typename FP_T, Eigen::StorageOptions storageLayout>
	BranchNodeContainer_SOA<FP_T, storageLayout> to_SOA(const BranchNodeContainer_AOS<FP_T>& nodesAOS) {

		BranchNodeContainer_SOA<FP_T, storageLayout> nodesSOA{ gsl::narrow_cast<uint32_t>(nodesAOS.size()) };

		const size_t numNodes = nodesAOS.size();

		for (size_t i = 0; i < numNodes; ++i) {
			nodesSOA.ids(i) = nodesAOS[i].id;

			nodesSOA.gridPositions(i, 0) = nodesAOS[i].gridPosition.x;
			nodesSOA.gridPositions(i, 1) = nodesAOS[i].gridPosition.y;
			nodesSOA.gridPositions(i, 2) = nodesAOS[i].gridPosition.z;

			for (size_t j = 0; j < nodesAOS[i].fineNodes.size(); ++j) {
				nodesSOA.fineNodes(i, j) = nodesAOS[i].fineNodes[j];
			}

			for (size_t j = 0; j < nodesAOS[i].adjNodes.size(); ++j) {
				nodesSOA.adjNodes(i, j) = nodesAOS[i].adjNodes[j];
			}

			for (size_t j = 0; j < nodesAOS[i].coarseNodes.size(); ++j) {
				nodesSOA.coarseNodes(i, j) = nodesAOS[i].coarseNodes[j];
			}
		}

		return nodesSOA;
	}

	template<typename FP_T, Eigen::StorageOptions storageLayout>
	LeafLevel_SOA<FP_T, storageLayout> to_leafLevel_SOA(const LeafLevel<FP_T>& leafLevel) {
		
		LeafLevel_SOA<FP_T, storageLayout> leafLevelSOA;
		leafLevelSOA.elements = to_SOA<FP_T, storageLayout>(leafLevel.elements);
		leafLevelSOA.nodes = to_SOA<FP_T, storageLayout>(leafLevel.nodes);
		return leafLevelSOA;
	}

	template<typename FP_T, Eigen::StorageOptions storageLayout>
	BranchLevel_SOA<FP_T, storageLayout> to_branchLevel_SOA(const BranchLevel<FP_T>& branchLevel) {

		BranchLevel_SOA<FP_T, storageLayout> branchLevelSOA;
		branchLevelSOA.nodes = to_SOA<FP_T, storageLayout>(branchLevel.nodes);

		return branchLevelSOA;
	}

	template<typename FP_T, Eigen::StorageOptions storageLayout>
	GMMMesh_SOA<FP_T, storageLayout> to_GMMMesh_SOA(const GMMMesh<FP_T>& gmmMesh) {
		
		GMMMesh_SOA<FP_T, storageLayout> gmmMeshSOA;

		gmmMeshSOA.leafLevel = to_leafLevel_SOA<FP_T, storageLayout>(gmmMesh.leafLevel);

		for (const auto& branchLevel : gmmMesh.branchLevels) {
			gmmMeshSOA.branchLevels.emplace_back(to_branchLevel_SOA<FP_T, storageLayout>(branchLevel));
		}

		return gmmMeshSOA;
	}

} // namespace gmm