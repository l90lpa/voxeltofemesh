
#pragma once

#include "GMMMesh_AOS.h"
#include "GMMMesh/impl/VoxToGMMMesh_impl.h"

#include <glm/vector_relational.hpp>

#include <tuple>

namespace gmm {

	template<typename FP_t, typename BitGrid_T, typename FP_Coord_T>
	GMMMesh<FP_t> voxToGMMMesh(VoxelGrid<BitGrid_T, FP_Coord_T> vGrid) {

		// Construct the leaf level (lv0) keeping the node position to index map (VolumeMesh) -> Nodes + NodeMap

		// While the mesh is still able to be coarsened
		//  - coarsen the mesh (mipmap)
		//  - construct the nodes for the coarser mesh (using an unordered_map to store the position to location table)
		//  - update the adjacency information for the current level nodes 
		//  - update the finer level information for the current level nodes
		//  - update the coarser level information for the previous level nodes

		using Node_Index_t = uint32_t;

		GMMMesh<FP_t> gmmMesh;

		auto [leafLevel, leafLevelNodeMap] = impl::constructSimulationLevelNodes<FP_t, Node_Index_t>(vGrid);

		if (leafLevel.nodes.empty()) {
			return gmmMesh;
		}

		gmmMesh.leafLevel = std::move(leafLevel);

		impl::updateLevelTopology<FP_t>(gmmMesh.leafLevel, leafLevelNodeMap);

		auto prevGridDims = vGrid.gridDimensions();
		auto currVGrid = downSample(vGrid);
		auto currGridDims = currVGrid.gridDimensions();

		std::vector<BranchLevel<FP_t>> branchLevels;

		if (glm::any(glm::lessThan(currGridDims, prevGridDims))) {

			auto [currCoarseLevel, currCoarseLevelNodeMap] = impl::constructCoarseLevelNodes<FP_t, Node_Index_t>(currVGrid);

			if (currCoarseLevel.nodes.empty()) {
				return gmmMesh;
			}

			impl::updateLevelTopology<FP_t>(currCoarseLevel, currCoarseLevelNodeMap);
			impl::updateInterLevelTopology<FP_t>(gmmMesh.leafLevel, leafLevelNodeMap, currCoarseLevel, currCoarseLevelNodeMap);
			branchLevels.emplace_back(std::move(currCoarseLevel));
			
			prevGridDims = currVGrid.gridDimensions();
			currVGrid = downSample(currVGrid);
			currGridDims = currVGrid.gridDimensions();

			auto prevCoarseLevelNodeMap = std::move(currCoarseLevelNodeMap);

			while (glm::any(glm::lessThan(currGridDims, prevGridDims))) {
				
				std::tie(currCoarseLevel, currCoarseLevelNodeMap) = impl::constructCoarseLevelNodes<FP_t, Node_Index_t>(currVGrid);

				if (currCoarseLevel.nodes.empty()) {
					return gmmMesh;
				}

				impl::updateLevelTopology<FP_t>(currCoarseLevel, currCoarseLevelNodeMap);
				impl::updateInterLevelTopology<FP_t>(branchLevels.back(), prevCoarseLevelNodeMap, currCoarseLevel, currCoarseLevelNodeMap);
				branchLevels.emplace_back(std::move(currCoarseLevel));

				prevGridDims = currVGrid.gridDimensions();
				currVGrid = downSample(currVGrid);
				currGridDims = currVGrid.gridDimensions();

				prevCoarseLevelNodeMap = std::move(currCoarseLevelNodeMap);
			}
		}

		gmmMesh.branchLevels = std::move(branchLevels);

		return gmmMesh;
	}


} // namespace gmm
