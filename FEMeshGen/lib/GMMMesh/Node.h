
#pragma once

#include <cstdint>
#include <array>

#include <glm/vec3.hpp>

namespace gmm {

	template<typename FP_T>
	class LeafNodeIC {
	public:
		glm::tvec3<FP_T> initialForce; // initial force
		bool is_fixedPosition;
	};


	template<typename FP_T>
	class LeafNode {
	public:	
		uint32_t id; // index
		glm::ivec3 gridPosition;

		// Topology
		std::array<int32_t, 8> elements;
		std::array<int32_t, 26> adjNodes;
		std::array<int32_t, 8> coarseNodes;

		glm::tvec3<FP_T> initialPosition; // initial position

		LeafNodeIC<FP_T> initialConditions;
	};



	// Overview:
	// 
	// 
	// Members:
	// - finerNodes: the 27 nearest nodes on the finer level wrt this given node
	// - adjNodes: the 26 nearest nodes within the given nodes level
	// - coarseNodes: the nodes on the coarse level 'within one-neighbourhood'
	//   - if the given node lies in the centre of an 'element' of the coarser level then it will have 8 coarseNodes
	//   - if the given node lies on a plane of an 'element' of the coarser level then it will have 4 coarseNodes
	//   - if the given node lies on an edge of an 'element' of the coarser level then it will have 2 coarseNodes
	//   - if the given node lies on a vertex of an 'element' of the coarser level then it will have 1 coarseNodes
	//   the remaining nodes are marked with -1.
	//
	template<typename FP_T>
	class BranchNode {
	public:
		uint32_t id; // index
		glm::ivec3 gridPosition;

		// Topology
		std::array<int32_t, 27> fineNodes;
		std::array<int32_t, 26> adjNodes;
		std::array<int32_t, 8> coarseNodes;
	};

} // namespace gmm