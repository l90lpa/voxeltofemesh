
#pragma once

#include <cstdint>
#include <array>

namespace gmm {

	template<typename FP_T>
	class ElementIC {
	public:
		FP_T elasticModulus;
		FP_T density;
	};

	template<typename FP_T>
	class Element {
	public:
		uint32_t   id;
		std::array<int32_t, 8> nodes;
		ElementIC<FP_T> initialConditions;
	};
} // namespace gmm