
#pragma once

#include <vector>

#include "GMMMesh/Element.h"

namespace gmm {

	template<typename FP_T>
	using ElementContainer_AOS = std::vector<Element<FP_T>>;

} // namespace gmm