
#pragma once

#include <cstdint>
#include <vector>

#include <glm/vec3.hpp>

#include "EigenTypes.h"
#include "GMMMesh/Node.h"

namespace gmm {


	template<typename FP_T, Eigen::StorageOptions StorageLayout>
	class LeafNodeICsContainer_SOA {
	public:
		Eigen::Matrix<FP_T, Eigen::Dynamic, 3, StorageLayout> initialForces;
		VectorCol<bool> is_fixedPositions;

		bool empty() const {
			return is_empty(initialForces) && is_empty(is_fixedPositions);
		}
	};


	template<typename FP_T, Eigen::StorageOptions StorageLayout>
	class LeafNodeContainer_SOA {
	public:
		using value_type = LeafNode<FP_T>;

		VectorCol<uint32_t> ids;
		Eigen::Matrix<int32_t, Eigen::Dynamic, 3, StorageLayout> gridPositions;

		// Topology
		Eigen::Matrix<int32_t, Eigen::Dynamic, 8, StorageLayout> elements;
		Eigen::Matrix<int32_t, Eigen::Dynamic, 26, StorageLayout> adjNodes;
		Eigen::Matrix<int32_t, Eigen::Dynamic, 8, StorageLayout> coarseNodes;

		Eigen::Matrix<FP_T, Eigen::Dynamic, 3, StorageLayout> initialPositions;

		LeafNodeICsContainer_SOA<FP_T, StorageLayout> initialConditions;

		LeafNodeContainer_SOA() = default;

		LeafNodeContainer_SOA(uint32_t numElements) {
			ids.resize(numElements, Eigen::NoChange);
			gridPositions.resize(numElements, Eigen::NoChange);
			elements.resize(numElements, Eigen::NoChange);
			adjNodes.resize(numElements, Eigen::NoChange);
			coarseNodes.resize(numElements, Eigen::NoChange);
			initialPositions.resize(numElements, Eigen::NoChange);
			initialConditions.initialForces.resize(numElements, Eigen::NoChange);
			initialConditions.is_fixedPositions.resize(numElements, Eigen::NoChange);
		}

		bool empty() const {
			return is_empty(ids) && is_empty(gridPositions) && is_empty(elements) && is_empty(adjNodes) && 
				is_empty(coarseNodes) && is_empty(initialPositions) && initialConditions.empty();
		}

		tl::expected<size_t, std::string> size() const {
			// Check that all the members have consistent dimensions

			auto testValue = ids.rows();

			bool are_consistent = (testValue == gridPositions.rows());
			are_consistent = (are_consistent && (testValue == elements.rows()));
			are_consistent = (are_consistent && (testValue == adjNodes.rows()));
			are_consistent = (are_consistent && (testValue == coarseNodes.rows()));
			are_consistent = (are_consistent && (testValue == initialPositions.rows()));
			are_consistent = (are_consistent && (testValue == initialConditions.initialForces.rows()));
			are_consistent = (are_consistent && (testValue == initialConditions.is_fixedPositions.rows()));

			if (are_consistent)
				return testValue;
			else
				return tl::make_unexpected(std::string{ "Inconsistent member sizes." });
		}
	};

	template<typename FP_T, Eigen::StorageOptions StorageLayout>
	LeafNode<FP_T> getNode(const size_t index, const LeafNodeContainer_SOA<FP_T, StorageLayout>& nodes) {

		LeafNode<FP_T> node;

		node.id = nodes.ids(index);

		for (size_t j = 0; j < nodes.gridPositions.cols(); ++j) {
			node.gridPosition[j] = nodes.gridPositions(index, j);
		}

		for (size_t j = 0; j < nodes.elements.cols(); ++j) {
			node.elements[j] = nodes.elements(index, j);
		}

		for (size_t j = 0; j < nodes.adjNodes.cols(); ++j) {
			node.adjNodes[j] = nodes.adjNodes(index, j);
		}

		for (size_t j = 0; j < nodes.coarseNodes.cols(); ++j) {
			node.coarseNodes[j] = nodes.coarseNodes(index, j);
		}

		for (size_t j = 0; j < nodes.initialPositions.cols(); ++j) {
			node.initialPosition[j] = nodes.initialPositions(index, j);
		}

		for (size_t j = 0; j < nodes.initialConditions.initialForces.cols(); ++j) {
			node.initialConditions.initialForce[j] = nodes.initialConditions.initialForces(index, j);
		}

		node.initialConditions.is_fixedPosition = nodes.initialConditions.is_fixedPositions(index);

		return node;
	}


	// Overview:
	// 
	// 
	// Members:
	// - finerNodes: the 27 nearest nodes on the finer level wrt this given node
	// - adjNodes: the 26 nearest nodes within the given nodes level
	// - coarseNodes: the nodes on the coarse level 'within one-neighbourhood'
	//   - if the given node lies in the centre of an 'element' of the coarser level then it will have 8 coarseNodes
	//   - if the given node lies on a plane of an 'element' of the coarser level then it will have 4 coarseNodes
	//   - if the given node lies on an edge of an 'element' of the coarser level then it will have 2 coarseNodes
	//   - if the given node lies on a vertex of an 'element' of the coarser level then it will have 1 coarseNodes
	//   the remaining nodes are marked with -1.
	//
	template<typename FP_T, Eigen::StorageOptions StorageLayout>
	class BranchNodeContainer_SOA {
	public:
		using value_type = BranchNode<FP_T>;

		VectorCol<uint32_t> ids;
		Eigen::Matrix<int32_t, Eigen::Dynamic, 3, StorageLayout> gridPositions;

		// Topology
		Eigen::Matrix<int32_t, Eigen::Dynamic, 27, StorageLayout> fineNodes;
		Eigen::Matrix<int32_t, Eigen::Dynamic, 26, StorageLayout> adjNodes;
		Eigen::Matrix<int32_t, Eigen::Dynamic, 8, StorageLayout> coarseNodes;

		BranchNodeContainer_SOA() = default;

		BranchNodeContainer_SOA(uint32_t numElements) {
			ids.resize(numElements, Eigen::NoChange);
			gridPositions.resize(numElements, Eigen::NoChange);
			fineNodes.resize(numElements, Eigen::NoChange);
			adjNodes.resize(numElements, Eigen::NoChange);
			coarseNodes.resize(numElements, Eigen::NoChange);
		}

		bool empty() const {
			return is_empty(ids) && is_empty(gridPositions) && is_empty(fineNodes) && is_empty(adjNodes) &&
				is_empty(coarseNodes);
		}

		tl::expected<size_t, std::string> size() const {
			// Check that all the members have consistent dimensions

			auto testValue = ids.rows();

			bool are_consistent = (testValue == gridPositions.rows());
			are_consistent = (are_consistent && (testValue == fineNodes.rows()));
			are_consistent = (are_consistent && (testValue == adjNodes.rows()));
			are_consistent = (are_consistent && (testValue == coarseNodes.rows()));

			if (are_consistent)
				return testValue;
			else
				return tl::make_unexpected(std::string{ "Inconsistent member sizes." });
		}
	};

	template<typename FP_T, Eigen::StorageOptions StorageLayout>
	BranchNode<FP_T> getNode(const size_t index, const BranchNodeContainer_SOA<FP_T, StorageLayout>& nodes) {

		BranchNode<FP_T> node;

		node.id = nodes.ids(index);

		for (size_t j = 0; j < nodes.gridPositions.cols(); ++j) {
			node.gridPosition[j] = nodes.gridPositions(index, j);
		}
		
		for (size_t j = 0; j < nodes.fineNodes.cols(); ++j) {
			node.fineNodes[j] = nodes.fineNodes(index, j);
		}

		for (size_t j = 0; j < nodes.adjNodes.cols(); ++j) {
			node.adjNodes[j] = nodes.adjNodes(index, j);
		}

		for (size_t j = 0; j < nodes.coarseNodes.cols(); ++j) {
			node.coarseNodes[j] = nodes.coarseNodes(index, j);
		}

		return node;
	}

} // namespace gmm