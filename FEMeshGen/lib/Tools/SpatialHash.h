
#pragma once 

#include <unordered_map>

#include <glm/vec3.hpp>

// specialized hash function for unordered_map keys
struct spatial_hash_r3
{
	template <class T>
	std::size_t operator() (const glm::tvec3<T>& key) const
	{
		std::size_t h1 = std::hash<T>()(key.x);
		std::size_t h2 = std::hash<T>()(key.y);
		std::size_t h3 = std::hash<T>()(key.z);

		return h1 ^ h2^ h3;
	}
};

template<typename FP_t, typename Value_t>
using SpatialHashTable_r3 = std::unordered_map<glm::tvec3<FP_t>, Value_t, spatial_hash_r3>;