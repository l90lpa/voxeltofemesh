
#include "FiniteSpaceTools.h"

#include <glm/common.hpp>
#include <glm/gtx/component_wise.hpp>
#include <gsl-lite.hpp>

uint32_t lMaxNorm(const glm::ivec3& positionA, const glm::ivec3& positionB) {
	return glm::compMax(glm::abs(positionA - positionB));
}

bool is_adjacent(const glm::ivec3& positionA, const glm::ivec3& positionB) {
	return lMaxNorm(positionA, positionB) < 2;
}


uint32_t index(const glm::uvec3& position, const glm::uvec3& spaceDimensions, SpatialOrdering::LexicographicXYZTag tag) {
	return (position.x + (position.y * spaceDimensions.x) + (position.z * spaceDimensions.x * spaceDimensions.y));
}

glm::uvec3 position(uint32_t index, const glm::uvec3& spaceDimensions, SpatialOrdering::LexicographicXYZTag tag) {
	uint32_t z = index / (spaceDimensions.x * spaceDimensions.y);
	index -= z * (spaceDimensions.x * spaceDimensions.y);

	uint32_t y = index / spaceDimensions.x;
	index -= y * spaceDimensions.x;

	uint32_t x = index;

	return glm::uvec3{ x, y, z };
}

uint32_t positionToLocalizedIndex(const glm::uvec3& queryPosition, const glm::uvec3& origin, const glm::uvec3& localSpaceDimensions, SpatialOrdering::LexicographicXYZTag tag) {

	Expects((localSpaceDimensions.x == localSpaceDimensions.y) && (localSpaceDimensions.x == localSpaceDimensions.z));
	Expects((localSpaceDimensions.x > 1) && ((localSpaceDimensions.x % 2) == 1));

	Expects(lMaxNorm(queryPosition, origin) <= (localSpaceDimensions.x / 2));

	const glm::uvec3 offset = localSpaceDimensions / glm::uvec3{ 2,2,2 };

	return index((queryPosition - origin) + offset, localSpaceDimensions, tag);
}

glm::uvec3 localizedIndexToPosition(const uint32_t queryIndex, const glm::uvec3& origin, const glm::uvec3& localSpaceDimensions, SpatialOrdering::LexicographicXYZTag tag) {

	Expects((localSpaceDimensions.x == localSpaceDimensions.y) && (localSpaceDimensions.x == localSpaceDimensions.z));
	Expects((localSpaceDimensions.x > 1) && ((localSpaceDimensions.x % 2) == 1));

	Expects(queryIndex < glm::compAdd(localSpaceDimensions));

	return position(queryIndex, localSpaceDimensions, tag);
}