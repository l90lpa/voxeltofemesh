#pragma once

#include <glm/vec3.hpp>


struct SpatialOrdering {
	static struct LexicographicXYZTag {} LexicographicXYZ;
	static struct MortonTag {} Morton;
};


uint32_t lMaxNorm(const glm::ivec3& positionA, const glm::ivec3& positionB);

bool is_adjacent(const glm::ivec3& positionA, const glm::ivec3& positionB);


uint32_t index(const glm::uvec3& position, const glm::uvec3& spaceDimensions, 
	SpatialOrdering::LexicographicXYZTag tag);

glm::uvec3 position(uint32_t index, const glm::uvec3& spaceDimensions, SpatialOrdering::LexicographicXYZTag tag);

uint32_t positionToLocalizedIndex(const glm::uvec3& queryPosition, const glm::uvec3& origin,
	const glm::uvec3& localSpaceDimensions, SpatialOrdering::LexicographicXYZTag tag);

glm::uvec3 localizedIndexToPosition(const uint32_t queryIndex, const glm::uvec3& origin,
	const glm::uvec3& localSpaceDimensions, SpatialOrdering::LexicographicXYZTag tag);