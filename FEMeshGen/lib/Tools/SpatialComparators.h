
#pragma once

#include <glm/vec3.hpp>

template<typename Coord_t>
inline bool lessThan_lexicographically(const glm::tvec3<Coord_t>& a, const glm::tvec3<Coord_t>& b) {
	if (a.z < b.z) {
		return true;
	}
	else if (a.z > b.z) {
		return false;
	}
	else {
		if (a.y < b.y) {
			return true;
		}
		else if (a.y > b.y) {
			return false;
		}
		else {
			return a.x < b.x;
		}
	}
}