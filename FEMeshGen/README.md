
## Notes:

### Element.h
1. When designing this I decided to choose to pass the NodesPerElement in as a non-type template argument.
This means that the system effectively will only support one element type and not mixed as these will then
be different types. If we want to support mixed element types that have the same data type then we could 
pass this in as a runtime variable and modify the container of nodes to be a vector instead.

2. We are using int32_t for the adjacency information of the nodes so that we can us -1 to represent 
unassigned. We should swap to uint32_t and use 0 for unassigned and index nodes/elements from 1.

### Node.h
1. same as .1 for Element.h

### VoxToFEMesh.h/cpp

1. The logic of the two outer loops is just for stepping through the voxels and identifying the next active
voxel. We should move this into an iterator of VolumeMesh

2. VolumeMesh needs to store a transformation, scale and probably an AABB so that we can retrieve the actual voxel
coordinates in float and not just the integer grid positions as we are doing at the moment.

3. remove the element map as it is not needed.

4. improve the hueristic factor used for reserving some memory for the nodeMap.

### General

1. add unit testing

2. use pre/post-conditions

### VoxToGPU_GMM_Mesh
